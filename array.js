(function ()
{
    function voidCallbackWrapperFactory(callback, thisArg) {
        return function (element, index, array) {
            if (thisArg !== undefined && thisArg !== null) {
                callback.call(thisArg, element, index, array);
            } else {
                callback(element, index, array);
            }
        };
    }

    function returningCallbackWrapperFactory(callback, thisArg) {
        return function (element, index, array) {
            if (thisArg !== undefined && thisArg !== null) {
                return callback.call(thisArg, element, index, array);
            }
            return callback(element, index, array);
        };
    }

    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function (callback, thisArg) {
            const wrapper = voidCallbackWrapperFactory(callback, thisArg);
            for (let i = 0; i < this.length; i++) {
                wrapper(this[i], i, this);
            }
        };
    }

    if (!Array.prototype.map) {
        Array.prototype.map = function (callback, thisArg) {
            const wrapper = returningCallbackWrapperFactory(callback, thisArg);
            const result = [];
            for (let i = 0; i < this.length; i++) {
                result.push(wrapper(this[i], i, this));
            }
            return result;
        };
    }

    if (!Array.prototype.filter) {
        Array.prototype.filter = function (callback, thisArg) {
            const wrapper = returningCallbackWrapperFactory(callback, thisArg);
            const result = [];
            for (let i = 0; i < this.length; i++) {
                if (wrapper(this[i], i, this)) {
                    result.push(this[i]);
                }
            }
            return result;
        };
    }

    if (!Array.prototype.reduce) {
        Array.prototype.reduce = function (callback, initialValue) {
            let result = initialValue === undefined ? this[0] : initialValue;
            const firstIndex = initialValue === undefined ? 1 : 0;
            for (let i = firstIndex; i < this.length; i++) {
                result = callback(result, this[i], i, this);
            }
            return result;
        };
    }

    if (!NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
            const wrapper = voidCallbackWrapperFactory(callback, thisArg);
            for (let i = 0; i < this.length; i++) {
                callback(this.item(i));
            }
        };
    }

    if (!NodeList.prototype.map) {
        NodeList.prototype.map = function (callback, thisArg) {
            const wrapper = returningCallbackWrapperFactory(callback, thisArg);
            const result = [];
            for (let i = 0; i < this.length; i++) {
                result.push(wrapper(this.item(i), i, this));
            }
            return result;
        };
    }

    if (!NodeList.prototype.filter) {
        NodeList.prototype.filter = function (callback, thisArg) {
            const wrapper = returningCallbackWrapperFactory(callback, thisArg);
            const result = [];
            for (let i = 0; i < this.length; i++) {
                const node = this.item(i);
                if (wrapper(node, i, this)) {
                    result.push(node);
                }
            }
            return result;
        };
    }

    if (!NodeList.prototype.reduce) {
        NodeList.prototype.reduce = function (callback, initialValue) {
            let result = initialValue === undefined ? this[0] : initialValue;
            const firstIndex = initialValue === undefined ? 1 : 0;
            for (let i = firstIndex; i < this.length; i++) {
                result = callback(result, this.item(i), i, this);
            }
            return result;
        };
    }
})();