//(function () {
    //if (!NewPromise) {
        const PromiseState = {
            WAITING: -1,
            REJECTED: 0,
            RESOLVED: 1
        };

        function NewPromise (executorCallback) {
            this.state = PromiseState.WAITING;
            this.value = null;
            this.nextPromise = null;

            const newPromise = this;
            function resolve(value) {
                newPromise.resolve(value);
            }
            function reject(err) {
                newPromise.reject(err);
            }

            if (executorCallback) {
                executorCallback(resolve, reject);
            }
        }

        NewPromise.prototype.resolve = function (value) {
            this.state = PromiseState.RESOLVED;
            this.value = value;
            if (this.nextPromise) {
                this.nextPromise.resolve(value);
            }
        };

        NewPromise.prototype.reject = function (err) {
            this.state = PromiseState.REJECTED;
            this.value = err;
            if (this.nextPromise) {
                this.nextPromise.reject(err);
            }
        };

        NewPromise.prototype.then = function (resolveCallback, rejectCallback) {
            if (this.state === PromiseState.RESOLVED) {
                try {
                    const newValue = resolveCallback(this.value);
                    if (isPromise(newValue)) {
                        this.nextPromise = newValue;
                        return newValue;
                    } else {
                        this.nextPromise = NewPromise.resolve(newValue);
                        return this.nextPromise;
                    }
                } catch (err) {
                    this.nextPromise = NewPromise.reject(err);
                    return this.nextPromise;
                }
            } else if (this.state === PromiseState.REJECTED) {
                try {
                    const newValue = rejectCallback(this.value);
                    if (isPromise(newValue)) {
                        this.nextPromise = newValue;
                        return newValue;
                    } else {
                        this.nextPromise = NewPromise.resolve(newValue);
                        return this.nextPromise;
                    }
                } catch (err) {
                    this.nextPromise = NewPromise.reject(err);
                    return this.nextPromise;
                }
            } else {
                const chainedPromise = new NewPromise();
                if (resolveCallback) {
                    chainedPromise.resolve = function (value) {
                        try {
                            const newValue = resolveCallback(value);
                            updateChainedPromise(chainedPromise, newValue);
                            if (chainedPromise.nextPromise) {
                                if (chainedPromise.status === PromiseState.RESOLVED) {
                                    chainedPromise.nextPromise.resolve(chainedPromise.value);
                                } else {
                                    chainedPromise.nextPromise.reject(chainedPromise.value);
                                }
                            }
                        } catch (err) {
                            chainedPromise.value = err;
                            chainedPromise.status = PromiseState.REJECTED;
                            if (chainedPromise.nextPromise) {
                                chainedPromise.nextPromise.reject(err);
                            }
                        }
                    };
                }
                if (rejectCallback) {
                    chainedPromise.reject = function (value) {
                        try {
                            const newValue = rejectCallback(value);
                            updateChainedPromise(chainedPromise, newValue);
                            if (chainedPromise.nextPromise) {
                                if (chainedPromise.status === PromiseState.RESOLVED) {
                                    chainedPromise.nextPromise.resolve(chainedPromise.value);
                                } else {
                                    chainedPromise.nextPromise.reject(chainedPromise.value);
                                }
                            }
                        } catch (err) {
                            chainedPromise.value = err;
                            chainedPromise.status = PromiseState.REJECTED;
                            if (chainedPromise.nextPromise) {
                                chainedPromise.nextPromise.reject(err);
                            }
                        }
                    };
                }
                this.nextPromise = chainedPromise;
                return chainedPromise;
            }
        };

        function updateChainedPromise(chainedPromise, value) {
            if (isPromise(value)) {
                chainedPromise.status = value.status;
                chainedPromise.value = value.value;
            } else {
                chainedPromise.value = value;
                chainedPromise.status = PromiseState.RESOLVED;
            }
        }

        function isPromise(instance) {
            return instance && instance.nextPromise !== undefined && instance.status !== undefined;
        }

        NewPromise.prototype.catch = function (rejectCallback) {
            return this.then(null, rejectCallback);
        };

        NewPromise.resolve = function (value) {
            const tmp = new ImmediatePromise(PromiseState.RESOLVED, value);
            return tmp;
        };

        NewPromise.reject = function (err) {
            return new ImmediatePromise(PromiseState.REJECTED, value);
        };



function ImmediatePromise (state, value) {
    this.state = state;
    this.value = value;
    this.nextPromise = null;
}
ImmediatePromise.prototype = NewPromise.prototype;
    //}
//})();